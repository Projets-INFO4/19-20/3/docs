# Polyserre by Idriss Sajide and Maxime Vernet

## Suivi 

# Avant la soutenance de mi-parcours du lundi 09/03

Nous avons d'abord apprit à coder avec Angular à l'aide d'un ide en ligne, StackLibtz, directement connecté à GitHub.
La première application que nous avons développé pour s'entrainer était un magasin en ligne avec un catalogue, un panier d'achat et un formulaire de paiement. Liens du tuto : https://angular.io/start

Ensuite nous avons modélisé notre application d'agriculture connectée à Polytech. Nous avons fait des maquettes et nous les avons présenté à 3 membres du BDH ( qui sont nos clients finalement ) ainsi qu'à un élève ingénieur de IESE3 qui va être amener à s'en servir aussi. Ces derniers nous ont apporté des recomandations que nous avons prit en compte, notament sur les listes de taches et les alertes.

Pour finir nous avons commencé à développer une première version de notre appliction, permettant de naviguer entre plusieurs serres.
Cela nous a necessiter d'apprendre à utiliser la librairie FireBase de Angular..

Problème:
Nous avons gardé tout ça sur un dépot GitHub trop longtemps et nous avons vraiment prit conscience de l'important de faire un suivi à partir de la soutenance de mi-parcours.. A partir du Lundi suivant nous avons prit des bonnes résolutions!

# Lundi 16/03 [Push]

Un nouveau départ.
Comme notre premier dépot GitLab était mal réalisé et que de toute manière nous n'avions fait qu'un seul push dessus alors nous avons décidé de créer un nouveau dépot "Projet-V2" en repartant sur de meilleurs bases avec un suivi et des push au jour le jour.

# Mardi 17/03 [Push]

Nous avons ajouté notre premier projet d'entrainement dans l'arboressence de nos fichiers et l'avons testé en utilisant 'ng serve'
Problèmes de compatibilité de passer d'un ide en ligne comme stackLibtz à utiliser npm dans la console..
Voir le nota bene ci-dessous pour apprendre à résoudre ce problème si cela vous arrive.

Nous avons également continué de nous documenter sur le language time script en lisant la suite des tuto sur StackLibtz.

# Mercredi 18/03

Nouvelle phase de discution et de modélisation de l'application.
Revu du cahier des charges. Nous devrions le rédiger et l'annexer dans le dossier docs lorsqu'il sera terminé.

# Jeudi 19/03 [Push]

Mise en place de styles avec CSS pour rendre nos premières pages comme sur nos maquettes.

Issue -> Nous avons rédigé une issue pour poser une question sur le dispositif à mettre en place pour les login & passewords

# REMARQUE SUR LES PUSH DU 19/03

En essayant de renomer un commit sur le depot git je me suis retrouvé avec 3 nouveaux commit, répliques d'anciens commit..

# Vendredi 20/03 [Push]

Création d'un nouveau component serre-new permettant de créer une nouvelle serre dans la base de donnée.
--> Formulaire à terminer
--> A terminer partie base de données

# Samedi 21/03 [Push]

Formulaire avancé + enregistrer la nouvelle serre et retour à la page principale.
--> Dans le formulaire il faut encore ajouter des taches et alertes.

# Dimanche 22/03 [Push]

Création d'un nouveau component add-serre permettant d'ajouter une serre dans sa liste à partir de son ID.
--> A terminer partie base de données

Amélioration des styles css des pages principales + ajout d'un boutton retour sur la page precedente

# Lundi 23/03 [Push]

Ajout de styles css : image en background, opacité + centrer les composents
Go back buttons

# Mardi 24/03

Dans le projet docs -> Mise à jour des Milestones et des Issues

# Mercredi 25/03 [Push]

On a répertorier l'ensemble des champs, taches et alertes que peuvent contenir une serre.
On a reconceptualisé la base de donnée.
Abandon d'une branche qui devait mettre en palce tous les champs d'une serre ( trop de modifications et trop d'erreurs ).

# Vendredi 27/03 [Push]

Début de la recherche sur l'Authentification.
Tests sur un exemple trouvé en open source.

# Dimanche 29/03 [Push]

Mise à jour requetes POST dans la base de donnée firebase.
+ Tests

# Lundi 30/03 [Push]

Création de nouveaux components permettants de signIn et signUp sur l'application.
A faire : Authentification

# Jeudi 02/04 [Push]

Tentative d'implementation de l'authentification.
Bugs à résoudre.

# Lundi 06/04

On continue à se documenter sur l'authentification HTTPS
Bugs non résolus..

# Mardi 07/04 [Push]

Résolution des bugs.
On peut désormais se connecter à l'application en utilisant le pseudo : test et le passeword : test
A faire : 
- Mettre en place une base de donnée liée aux actions signIn et signUp
- Affecter des serres à un utilisateur

# Mercredi 08/04 [Push]

- Responsive-design et ajout de boutons SignIn et SignUp
- Authentification amélioration

# Jeudi 09/04 [Push]
- Authentification terminée

# Samedi 11/04 [Push]
- New user
- Update user

# Mardi 14/04 [Push]
- Rapports : nouveaux champs

# Mercredi 15/04 [Push]
- Firebase serres services

# Mardi 21/04 [Push]
- Rapports details

# Mardi 28/04 [Push]
- Rapports details
- Grafana ( Erreur autorisation )

# Mercredi 29/04 [Push]
- Add serre

# Jeudi 30/04 [Push]
- Edit / Delete rapport


## TO DO LIST

- Grafana
- Alerts

- Jhipster: Angular, MySQL Poqtgres, API Rest
- Docker


## Usage 

# Npm
Tout d'abord l'instalaltion de npm est requise avec la commande `npm install`

# Firebase
Il faut également installer la librairie firebase permettant à notre application d'avoir sa propre base de donnée
`npm i @angular/fire` et puis `npm i firebase`

# Npm start
Vous pouvez lancer l'application à l'aide de la commande npm start depuis la recine du projet polyserre
Si cela ne fonctionne pas, essayez avec ng serve comme suit :

# Lancer un projet avec `ng serve` pour la première fois
Lorsque l'on clone le projet via le git on ne clone que ses sources.
On doit donc commencer par créer un projet avec angular `ng new <project-name>` et copier les dossiers APP et ENVIRONEMENTS des sources clonées au projet créé.

# Pour ouvrir la page web dans son navigateur internet :
Dans la console utiliser `ng serve` pour lancer l'application
Taper `localhost:4200` en url dans votre moteur de recherche

# Pour ouvrir l'application sur son smartphone :
Dans la console utiliser `ng serve --host [@IPV4deVotreReseauWifi]` pour lancer l'application
Se conecter au même reseau wifi sur votre smartphone
Taper `[@IPV4deVotreReseauWifi]:4200` en url dans votre moteur de recherche
Cela fonctionne sur tout appareil connecté sur votre wifi.


## Documentation officielle utilisation des commandes npm ##

# Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

# Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

# Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

# Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

# Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

# Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).





